from django import forms
from todos.models import TodoList, TodoItem
from django.forms import DateInput

class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']

class TodoItemCreate(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list',
        ]
        widgets = {
            'due_date': DateInput(),
        }
