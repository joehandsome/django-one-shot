from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemCreate

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()

    # print("Todo lists:", todo_lists)
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'todo/list.html', context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    context = {
        'todo_list': todo_list,
        'todo_items': todo_items,
    }
    return render(request, 'todo/detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoListForm()
    context = {
        'form': form,
    }
    return render(request, 'todo/create.html', context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance = todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        'form' : form,
        }
    return render(request, 'todo/edit.html', context)

def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    return render(request, 'todo/delete.html')

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemCreate(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.id)
    else:
            form = TodoItemCreate()
    context = {
            'form': form,
        }
    return render(request, 'todo/item.html', context)

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemCreate(request.POST, instance = todo_item)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.id)
    else:
        form = TodoItemCreate(instance=todo_item)
    context = {
        'form':form
    }
    return render(request, 'todo/update.html', context)
